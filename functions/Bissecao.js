function calcula(x, expressao) {
    return eval(expressao.replace("x", x));
}

//<========================== PRECISAO
////INTERVALO [a,b]
// var a = 0.5; //<=================================== INTERVALOS
// var b = 2;


function Bissecao(expressao, a, b, PRECISAO) {


    var ValoresCalculo_a = [];
    var ValoresCalculo_b = [];
    var ValoresCalculo_Fx = [];
    var ValoresCalculo_Precisao = [];


    var E = Math.pow(10, PRECISAO);
    var x;
    var xn = 0;
    var precisao;
    var interacao = 0;
    var fx;
    console.log(a);




    var fa = calcula(a, expressao);
    console.log(fa);

    var fb = calcula(b, expressao);

    console.log(fb);

    precisao = Math.abs(b - a);

    if (fa < 0 && fb > 0) {
        while (precisao >= E) {
            interacao++;
            xn = ((a + b) / 2);


            fx = calcula(xn, expressao);
            if (fx < 0) {

                a = xn;


            } else {
                b = xn;
            }
            precisao = Math.abs(b - a);
            // printf("=================================================\n");
            // printf("interacao = %d \n", interacao);
            // printf("a = %.20lf \n", a);
            // printf("b = %.20lf \n", b);
            // printf("x%d = %.20lf \n", interacao, xn);
            // printf("f(x%d) = %.20lf \n", interacao, fx);
            // printf("[%.20lf ; %.20lf] \n", a, b);
            // printf("|b-a|= %.20lf \n", precisao);
            console.log(a);
            ValoresCalculo_a.push(a);
            ValoresCalculo_b.push(b);
            ValoresCalculo_Fx.push(fx);
            ValoresCalculo_Precisao.push(precisao);


        }

    } else if (fa > 0 && fb < 0) {
        console.log(precisao)
        while (precisao >= E) {
            interacao++;
            xn = ((a + b) / 2);
            fx = calcula(xn, expressao);
            if (fx < 0) {

                b = xn;
            } else {
                a = xn;
            }
            precisao = Math.abs(b - a);
            // printf("=================================================\n");
            // printf("interacao = %d \n", interacao);
            // printf("a = %.20lf \n", a);
            // printf("b = %.20lf \n", b);
            // printf("x%d = %.20lf \n", interacao, xn);
            // printf("f(x%d) = %.20lf \n", interacao, fx);
            // printf("[%.20lf - %.20lf] \n", a, b);
            // printf("|b-a|= %.20lf \n", precisao);

            ValoresCalculo_a.push(a);
            ValoresCalculo_b.push(b);
            ValoresCalculo_Fx.push(fx);
            ValoresCalculo_Precisao.push(precisao);
        }

    }


    var ValoresCalculo = {
        ValoresCalculo_a: ValoresCalculo_a,
        ValoresCalculo_b: ValoresCalculo_b,
        ValoresCalculo_Fx: ValoresCalculo_Fx,
        ValoresCalculo_Precisao: ValoresCalculo_Precisao
    }

    mostrarResultado(ValoresCalculo, 1);
}